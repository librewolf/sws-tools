#### Install 

```sh
~$ git clone https://git.disroot.org/librewolf/sws-tools.git
~$ cd sws-tools
~$ cp .env.example .env && nano .env
~$ docker-compose up -d
````